(function () {
	'use strict';
	const $ = (selector) => document.querySelector(selector);

	let maxObjects = 9;

	let app = {
		data: [],
		onInit: function() {
			let buttons = [null, "create", "view", "remove"];
			for (let i = 1; i < buttons.length; i++)	{
				let buttonName = buttons[i].charAt(0).toUpperCase() + buttons[i].slice(1);
				$('button[name="button-'+buttons[i]+'"]').innerHTML = buttonName;
			};

			$('button[name="button-create"]').addEventListener('click', () => this.onCreate());
			$('button[name="button-view"]').addEventListener('click', () => this.onView());
			$('button[name="button-remove"]').addEventListener('click', () => this.onRemove());
		},
		onCreate: function() {
			if (this.data.length < maxObjects) {
				let dateOptions = { 
					date: { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' },
					time: { hour12: false },
				};
				let today = new Date();
				let localDate = today.toLocaleDateString('en-US', dateOptions.date)
				let localTime = today.toLocaleTimeString('en-US', dateOptions.time)
				function getRandom(min, max) {
  					min = Math.ceil(min);
  					max = Math.floor(max);
  					return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
				}
				this.data.push({ 
					Time: localTime, 
					Date: localDate, 
					Num: getRandom(100,999), 
				});
				this.onUpdate();
				this.onConsole('Created an Object inside the array.');
				//console.log('Created an Object inside the array.');
			} else {
				this.onConsole(`You cannot create more than ${maxObjects} Objects.`);
				//console.log(`You cannot create more than ${maxObjects} Objects`)
			};
		},
		onView: function() {
			alert(JSON.stringify(this.data[this.data.length - 1]));
			console.log('Alerted the last object inside the array.');
		},
		onRemove: function() {
			if (this.data.length > 0) {
				this.data.pop(this.data[this.data.length - 1]);
				this.onUpdate();
				this.onConsole('Removed the last object from an array.',success);
				//console.log('Removed the last object from an array.');
			}else {
				this.onConsole('The array is empty, nothing to remove.');
				//console.log('The array is empty, nothing to remove.');
			};
		},
		onConsole: function(msg, status) {
			status = success ? `<span class="error">Error</span>` : `<span class="error">Success</span>`;
			msg = status ? status + ':' + msg : msg;
			$('#console div').innerHTML = '';
			$('#console div').innerHTML = `<p class="console">${msg}</p>`;
		},
		onUpdate: function() {
			$('ul').innerHTML = '';
			let boxStyle = [ "flat-red", "flat-green", "flat-blue"];
			for (let i in this.data) {
				let data = this.data[i]
				$('ul').innerHTML += 
				`<li class="${boxStyle[i % boxStyle.length]}">
					<span class="headline">Time</span> <span>${data.Time}</span>
					<span class="headline">Date</span> <span>${data.Date}</span>
					<span class="headline">Number</span> <span>${data.Num}</span> 
				</li>`;
			};
		},
	};
	document.addEventListener("DOMContentLoaded", function(event) {
		app.onInit();
    	console.log("DOM fully loaded and parsed");
	});
})();